# Per.js
此项目已经启用了码云gitee page服务，使用“ http://per-js.skyogo.com/版本号/Per.js ”链接即可在线获取JS文件【你也可以把版本号替换成“new”，这样就能直接链接到最新发行版的Per.js，类似：“ http://per-js.skyogo.com/new/Per.js ”】<br>
同时欢迎查看和Vue.js的速度对比：https://gitee.com/skyogo/Per.js/blob/master/速度对比VueJS.md<br>
#### 项目介绍
#### Per.js - 快速、简便的响应式JavaScript开发框架。

Per.js是一个开源的渐进式+响应式的大型JavaScript开发框架，他拥有一系列简便的DOM操作函数，例如Vue的模板渲染、双向绑定等等。

同时他的执行速度还是Vue的7~8倍。

尽管他的DOM操作已经如此方便，但是他不只可以作用于DOM操作上。您还可以使用他的Ajax、Component（组件）等等操作。

![图](https://images.gitee.com/uploads/images/2018/0917/195420_a07c8733_1687981.gif "在这里输入图片标题")

- 他可以有效的帮助你减少需要编写的代码量
- 他完全是开源可扩展的
- 他的执行速度几乎是Vue.js的8~7倍

#### 浏览器支持
- IE >= 9
- Chrome >= 4.0
- Firefox >= 4.0
- Safari >= 5.0
- Opera >= 10.0

#### 安装教程

1. 下载Per.js文件
2. 使用script标签引入
3. 完成

#### 使用说明

详情请参见wiki里面的使用方法！

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request